import React from 'react';
import AddProduct from './AddProduct';

export class ProductList extends React.Component {
    constructor(){
        super();
        this.state = {
            products: []
        };
    }
    
    onItemAdded = (product) => {
        this.setState({
            products: [...this.state.products, product]
        })
    }
    
    render(){
        return(
            <div>
                <AddProduct onAdd={this.onItemAdded}/>
            </div>
        )
    }
}