import React from 'react';

export class AddProduct extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            category: '',
            price: ''
        };
    }

    addProduct = () => {
        let product = {
            name: this.state.name,
            category: this.state.category,
            price: this.state.price
        };
        this.props.onAdd(product);
    }
    
    handleChangeName = (event) => {
        this.setState({
            name: event.target.value
        })
    }
    
    handleChangeCategory = (event) => {
        this.setState({
            category: event.target.value
        })
    }
    
    handleChangePrice = (event) => {
        this.setState({
            price: event.target.value
        })
    }
        
    handleProductAdd = () => {
        this.props.onAdd(this.state);
    }

    render(){
        return (
            <div>
                <input type="text" id="name" name="name" onChange={this.handleChangeName}/>
                <input type="text" id="category" name="category" onChange={this.handleChangeCategory} />
                <input type="text" id="price" name="price" onChange={this.handleChangePrice}/>
                <input type="button" value="add product" onClick={this.handleProductAdd}/>
            </div>
        )
    }
}

export default AddProduct;